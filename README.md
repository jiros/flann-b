# FlaNN-B

A dockerised Flask Nginx Neo4j empty project. An exercise in learning Flask, Python and Docker. This is not a tutorial and these are my notes to help me keep track of things.

## Creating local environment
These steps setup the local environment and virtual environment prior to docker setup. This only needs to be done once and not at all if cloning the project rather than starting from scratch. Creating a local venv allows for the initial installation of python requirements and for the requirements.txt file to be generated.

- Create a github/gitlab repository
- Clone the blank repository to local pc
- cd /cloned/repository

- create a virtual environment
```
python3 -m venv ./venv
```
- Activate the virtual environments (deactivate when you're done)
```
source ./venv/bin/activate
```
```
Deactivate
```

## Setting up flask

With the venv activated install flask and gunicorn_config
```
pip install flask gunicorn
```

## Environment variables
To pass environment variables to a docker container use an .env file
So for the development server create a dev-flask.env file and add:
```
FLASK_APP=web.app_package
FLASK_ENV=Development
```
Then use dev-docker-compose with command: flask running

```
docker-compose -f dev-docker-compose.yml --env-file dev-flask.env up --build
```

## Development server
There are two ways to run a development server which can help debug issues along the ways
### In the local venv
This bybasses docker completely and you are interacting with the app directly.
- In a terminal.
```
export FLASK_APP=web.app_package
export FLASK_ENV=Development
flask run
```

### Using the flask server on a docker container
This builds a docker container to run the app but bypasses Nginx by having a simplified dev-docker-compose file that only builds the web service.
```
docker-compose -f dev-docker-compose.yml --env-file dev-flask.env up --build
```

## Live server
gunicorn is installed on the web container using requirements.txt
Gunicorn config is saved in /web/gunicorn_config.py which is then copied to the docker container by the dockerfile on build and called by docker-compose using
```
command: /usr/local/bin/gunicorn --config /web/gunicorn_config.py 'web.app_package:create_app()'
```
- https://docs.gunicorn.org/en/stable/run.html

```
docker-compose up --build
```

## Blueprints
One 'main' blueprint currently in the root folder.

## bootstrap
Based on Cover example with few modifications:
- added Logo
- removed some of the custom css
- Bootstrap 5 (beta)
- https://www.creative-tim.com/blog/web-design/add-bootstrap-html-guide/
- https://getbootstrap.com/docs/5.0/examples/cover/

## Gitignore and Dockerignore
To avoid pushing sensitive or unwanted files to gitlab/github or copying them to your docker containers these can be added to the .gitignore and .Dockerignore files. Create them if they don't exist.


## docker

- To see local images
```
docker images
```
- To see local containers
```
docker ps -a
```
- To stop a container
```
docker stop container_name
```
- To remove stopped containers
```
docker container prune
```
- To remove dangling images (old version of a newly built image)
```
docker image prune
```
- To remove unused and dangling images
```
docker image prune -a
```

## SSL configuration

Currently done on deployment platform - e.g. Azure or AWS

## Azure cli
run from root of repo
```
docker run -it -v $(pwd):/data/app mcr.microsoft.com/azure-cli
```
- https://docs.microsoft.com/en-us/cli/azure/run-azure-cli-docker?view=azure-cli-latest
- https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt

## Gitlab Registry

Create a personal access token
The landing page on Gitlab provides commands to login to registry and build/tag images

### Tags
Note that the image tag needs to match gitlab's conventions.
i.e. registry.gitlab.com/<org_name>/<project_name>/<image>:latest

## References

#### flask
- https://flask.palletsprojects.com/en/1.1.x/tutorial/

#### Python
- https://python.land/virtual-environments/virtualenv#Python_venv_activation
- https://docs.python.org/3/library/venv.html

#### Docker & Docker-Compose
- http://www.patricksoftwareblog.com/how-to-configure-nginx-for-a-flask-web-application/
- https://dev.to/holla22/create-a-docker-multi-container-flask-app-13fj
- https://www.patricksoftwareblog.com/how-to-use-docker-and-docker-compose-to-create-a-flask-application/
- https://medium.com/@thibaut.deveraux/how-to-install-neo4j-with-docker-compose-36e3ba939af0
- https://dev.to/marounmaroun/running-docker-container-with-gunicorn-and-flask-4ihg
- https://dev.to/ishankhare07/nginx-as-reverse-proxy-for-a-flask-app-using-docker-3ajg

- https://linuxhandbook.com/remove-docker-images/

- https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file
